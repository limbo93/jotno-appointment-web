export class AppointmentConfig {
  doctorId: number;
  orgId: number;
  orgCode: string;
  configId: number;
  status: string;
  timePerPatient: number;
  fee: number;
  followupFee: number;
  reportFee: number;
  availabilityType: string;
  appointmentRequestType: string;
  autoConfirmationEnabled: boolean;
  waitingListEnabled: boolean;
  waitingListSize: null;
  leaves: any[];
}
export class Appointment {
  id: number;
  orgId: number;
  orgCode: string;
  bmdc: string;
  doctorId: number;
  doctorType: string;
  date: string;
  time: string;
  serial: number;
  type: string;
  status: string;
  phoneNumber: string;
  name: string;
  dateOfBirth: string;
  gender: string;
  bloodGroup: string;
  emergency: boolean;
  patientComments: string;
  orgComments: string;
  scheduleId: number;
  patientProfileId: string;
  patientToBeRegistered: boolean;

  constructor() {
    this.type = 'NEW';
    this.gender = 'M';
    this.dateOfBirth = null;
  }
}