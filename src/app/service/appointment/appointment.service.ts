import { Appointment } from './domain/appointment.domain';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService, UrlPathParameters } from '../base.service';

const FETCH_DOCTOR_APPOINTMENT_CONFIG_BY_ORG_AND_DOCTOR = '/appointments/org/{orgId}/config/{doctorId}';
const BOOK_APPOINTMENT = '/appointments';

@Injectable()
export class AppointmentService extends BaseService {

    constructor(private httpClient: HttpClient) {
        super();
    }

    public fetchAppointmentConfigByOrganizationAndDoctor(urlPathParams: UrlPathParameters): Observable<any> {
        const url = this.create(FETCH_DOCTOR_APPOINTMENT_CONFIG_BY_ORG_AND_DOCTOR, urlPathParams);
        return this.httpClient.get(url);
    }

    public bookAppointment(appointment: Appointment): Observable<any> {
        return this.httpClient.post(BOOK_APPOINTMENT, appointment);
    }

}