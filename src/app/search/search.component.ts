import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
import { SelectItem } from 'primeng/api/selectitem';
import { NotificationService } from '../core/notification/notification.service';
import { Appointment } from '../service/appointment/domain/appointment.domain';
import { AppointmentService } from '../service/appointment/appointment.service';
import { NAME_REGEX, MOBILE_NUM_REGEX } from '../shared/constants/common.global.constants';
import { GenderOptions, BloodGroupsSortForm } from '../shared/domain/common.enum.models';
import { BaseComponent } from '../shared/components/base.component';

class Item {
  doctorId: number;
  doctorName: string;
  orgId: number;
  orgName: string;
  degree: string[];
  specilization: string[]
}

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent extends BaseComponent implements OnInit {

  item: Item;
  DAYS: string[] = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
  busyState: boolean = false;
  isDialogVisiable: boolean = false;

  selectedDate: Date = null;

  schedules: Map<string, SelectItem[]> = new Map([]);
  selectedDaySchedules: SelectItem[] = [];
  selectedSchedule: number = null;

  appointmentConfiguration: any;
  daysInAppointmentConfig: number[] = [];
  appointment: Appointment = new Appointment();
  bookedAppointment: Appointment = new Appointment();

  disabledDays: number[];
  disabledDates: Date[] = [];
  minDate: Date = new Date();
  maxDate: Date = moment(new Date()).add(3, 'M').toDate();
  maxBirthDate: Date = new Date();

  genders: SelectItem[] = GenderOptions;
  types: SelectItem[] = [{ label: 'New', value: 'NEW' }, { label: 'Followup', value: 'FOLLOW_UP' }, { label: 'Report', value: 'REPORT' }];
  bloodGroups: SelectItem[] = BloodGroupsSortForm;
  emergencies: SelectItem[] = [{ label: 'True', value: true }, { label: 'False', value: false }];

  phoneRegex: RegExp = MOBILE_NUM_REGEX;
  nameRegex: RegExp = NAME_REGEX;

  items: Item[] = [ //mock data
    {
      doctorId: 1,
      doctorName: 'Hasan',
      orgId: 1,
      orgName: 'Popular Hospital Ltd',
      degree: ['MBBS', 'FCPS'],
      specilization: ['Nak', 'Kan', 'Gola']
    },
    {
      doctorId: 12,
      doctorName: 'Sharaiar',
      orgId: 1,
      orgName: 'Popular Hospital',
      degree: ['MBBS', 'FCPS'],
      specilization: ['Nak', 'Kan', 'Gola']
    },
    {
      doctorId: 11,
      doctorName: 'Ali Ahmed',
      orgId: 1,
      orgName: 'Popular Hospital Ltd',
      degree: ['MBBS', 'FCPS'],
      specilization: ['Nak', 'Kan', 'Gola']
    },
    {
      doctorId: 81,
      doctorName: 'Ahmed',
      orgId: 1,
      orgName: 'Popular Hospital Ltd',
      degree: ['MBBS', 'FCPS'],
      specilization: ['Nak', 'Kan', 'Gola']
    },
    {
      doctorId: 14,
      doctorName: 'John Ramboo',
      orgId: 1,
      orgName: 'Popular Hospital Ltd',
      degree: ['MBBS', 'FCPS'],
      specilization: ['Nak', 'Kan', 'Gola']
    },
  ];

  filtereditems: Item[];

  @ViewChild('bookingForm') bookingForm: NgForm;

  constructor(private notificationService: NotificationService,
    private appointmentService: AppointmentService) {
    super();
  }

  ngOnInit(): void {
    this.test() //tesing purpose
  }

  filterItem(event) {
    let query = event.query;
    this.selectedDate = null;
    this.selectedSchedule = null;
    this.filtereditems = this.filterFinal(query, this.items);
  }

  filterFinal(query, items: Item[]): Item[] {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: Item[] = [];
    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      if (item.doctorName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(item);
      }
    }
    return filtered;
  }

  onDateSelect(date) {
    this.selectedSchedule = null;
    this.selectedDaySchedules = [];
    let schedules = this.schedules.get(this.DAYS[date.getDay()]);

    if (schedules) {
      schedules.sort((first, second) => {
        return moment(first.label.split('-')[0].trim(), 'hh:mm a').diff(moment(second.label.split('-')[0].trim(), 'hh:mm a'));
      });
      this.selectedDaySchedules = [{ label: 'Select a schedule', value: null }, ...schedules];
    }
  }

  //remove it if u load config from api call
  test() {
    let data = JSON.parse("{\"status\":\"SUCCESS\",\"data\":{\"config\":{\"autoConfirmation\":false,\"reportFee\":300,\"followupFee\":500,\"timePerPatient\":15,\"fee\":2000,\"waitingListEnabled\":false,\"availability\":\"Full Time\",\"waitingListSize\":0,\"type\":\"Slot\"},\"doctor\":{\"bmdc\":\"test1\",\"name\":\"Flora\"},\"leaves\":[],\"organization\":{\"code\":\"test\",\"address\":\"Hs#7, Rd#20, Sec#4, UTTARA\",\"name\":\"Test Organization\"},\"schedules\":[{\"from\":\"10:14 AM\",\"id\":48,\"day\":\"MONDAY\",\"to\":\"10:14 PM\"},{\"from\":\"12:34 PM\",\"id\":61,\"day\":\"SUNDAY\",\"to\":\"04:34 PM\"},{\"from\":\"02:00 PM\",\"id\":62,\"day\":\"TUESDAY\",\"to\":\"06:00 PM\"},{\"from\":\"05:13 PM\",\"id\":63,\"day\":\"WEDNESDAY\",\"to\":\"11:14 PM\"}]},\"msg\":null,\"errors\":null,\"fieldsErrors\":null,\"extra\":null}");
    console.log(data)
    this.appointmentConfiguration = data.data;
    this.configureAppointmentConfiguration();
    this.generateScheduleList();
  }

  //actual api call for loading config
  fetchAppointmentConfigByOrganizationAndDoctor(orgId: number, doctorId: number) {
    this.appointmentService
      .fetchAppointmentConfigByOrganizationAndDoctor({ orgId, doctorId })
      .subscribe(data => {
        this.appointmentConfiguration = data.data;
        this.configureAppointmentConfiguration();
        this.generateScheduleList();
      });
  }

  configureAppointmentConfiguration() {
    if (!this.appointmentConfiguration) return;

    this.appointmentConfiguration.schedules.forEach(schedule => {
      let daynumber = this.DAYS.indexOf(schedule.day);
      if (!this.daysInAppointmentConfig.includes(daynumber)) {
        this.daysInAppointmentConfig.push(daynumber);
      }
    });

    this.disabledDays = [0, 1, 2, 3, 4, 5, 6].filter((day) => {
      return this.daysInAppointmentConfig.indexOf(day) < 0;
    });

    let calculatedDisableDates = [];

    this.appointmentConfiguration.leaves.forEach(leave => {
      let startDate = moment(leave.from, 'DD/MM/YYYY hh:mm A').toDate();
      let endDate = moment(leave.to, 'DD/MM/YYYY hh:mm A').toDate();
      calculatedDisableDates = calculatedDisableDates.concat(this.getAllDatesBetweenTwo(startDate, endDate));
    });
    this.disabledDates = [...calculatedDisableDates];
  }

  getAllDatesBetweenTwo(startDate, endDate) {
    let dates = [];
    let currentDate = moment(startDate).startOf('day');
    let lastDate = moment(endDate).startOf('day');
    dates.push(currentDate.clone().toDate());
    while (currentDate.add(1, 'days').diff(lastDate) <= 0) {
      dates.push(currentDate.clone().toDate());
    }
    return dates;
  }

  generateScheduleList() {
    if (!this.appointmentConfiguration || !this.appointmentConfiguration.schedules || this.disabledDays.length === 7) {
      this.notificationService.sendErrorMsg('Appointment configuration has not set yet.');
      return;
    }

    this.schedules = new Map([]);
    this.appointmentConfiguration.schedules.forEach(schedule => {
      let scheduleSelectItem = { label: schedule.from + '-' + schedule.to, value: schedule.id };
      let schedules = this.schedules.get(schedule.day);
      if (schedules) {
        schedules = [...schedules, scheduleSelectItem];
      } else {
        schedules = [scheduleSelectItem];
      }
      this.schedules.set(schedule.day, schedules);
    });
  }

  bookAppointment() {
    if (this.bookingForm.invalid || !this.item || !this.item.doctorId) {
      this.notificationService.sendErrorMsg('Form invalid');
      return;
    };

    this.enableBusyState();
    this.appointment.doctorId = this.item.doctorId;
    this.appointment.orgId = this.item.orgId;
    this.appointment.date = moment(this.selectedDate).format('DD/MM/YYYY');
    this.appointment.scheduleId = this.selectedSchedule;

    // service call not tested yet
    this.appointmentService.bookAppointment(this.appointment).subscribe(
      res => {
        this.disableBusyState();
        this.notificationService.sendSuccessMsg('Appointment created successfully');
        this.clearForm();
        this.showConfirmation();
      },
      error => {
        if (error.error.errors && error.error.errors.length > 0) {
          this.notificationService.sendErrorMsg(error.error.errors[0]);
        }
        this.disableBusyState();
      });
  }

  clearForm() {
    this.selectedDate = null;
    this.selectedSchedule = null;
    this.selectedDaySchedules = [];
    this.item = new Item();
    this.appointment = new Appointment();
    this.markFormGroupAsUnTouched(this.bookingForm.form);
  }

  showConfirmation() {
    this.isDialogVisiable = true;
    // mock data. it will be filled with the response of book appointment
    let data = JSON.parse("{\"status\":\"SUCCESS\",\"data\":{\"id\":1202,\"orgId\":2,\"orgCode\":\"test\",\"bmdc\":\"test1\",\"doctorId\":38,\"doctorType\":\"Medical\",\"date\":\"07\/04\/2020\",\"time\":\"02:45 PM\",\"serial\":4,\"scheduleId\":62,\"type\":\"Followup\",\"status\":\"Pending\",\"phoneNumber\":\"01677719924\",\"name\":\"Rezaur Rahman\",\"dateOfBirth\":\"17\/07\/1989\",\"gender\":\"Male\",\"bloodGroup\":null,\"year\":null,\"month\":null,\"day\":null,\"emergency\":false,\"patientComments\":null,\"orgComments\":null,\"created\":\"07\/04\/2020 08:38 PM\",\"patientProfileId\":\"P-VHTCVUQ8X\"},\"msg\":null,\"errors\":null,\"fieldsErrors\":null,\"extra\":null}");
    this.bookedAppointment = data.data;
  }
}
