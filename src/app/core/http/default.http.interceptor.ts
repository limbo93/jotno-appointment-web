import {
    HttpErrorResponse,
    HttpHandler,
    HttpHeaderResponse,
    HttpInterceptor,
    HttpProgressEvent,
    HttpRequest,
    HttpResponse,
    HttpSentEvent,
    HttpUserEvent
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { environment } from './../../../environments/environment';


@Injectable()
export class DefaultInterceptor implements HttpInterceptor {

    constructor(private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler):

        Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {

        let url = req.url;

        if (!url.startsWith('https://') && !url.startsWith('http://')) {
            url = environment.API_BASE_URL + url;
        }

        const newReq = req.clone({ url, withCredentials: true });

        return next.handle(newReq).pipe(
            mergeMap((event: HttpResponse<any>) => {
                return this.handleData(event);
            }),
            catchError((error: HttpErrorResponse) => {
                this.handleError(error);
                return throwError(error);
            }));
    }

    private handleData(event: HttpResponse<any>): Observable<any> {

        switch (event.status) {
            case 200:
                break;
            case 201:
                break;
        }
        return of(event);
    }

    handleError(event: HttpErrorResponse) {
        switch (event.status) {
            case 400:
                this.handle400Error(event);
                break;
            case 401:
                this.handle401Error(event);
                break;
            case 403:
                this.handle403Error(event);
                break;
            case 404:
                this.handle404Error(event);
                break;
            case 500:
                this.handle500Error(event);
                break;
        }
        return of(event);
    }


    // Error handlers
    handle400Error(event: HttpResponse<any> | HttpErrorResponse) {

    }

    handle401Error(event: HttpResponse<any> | HttpErrorResponse) {

    }

    handle403Error(event: HttpResponse<any> | HttpErrorResponse) {

    }

    handle404Error(event: HttpResponse<any> | HttpErrorResponse) {

    }

    handle405Error(event: HttpResponse<any> | HttpErrorResponse) {

    }

    handle409Error(event: HttpResponse<any> | HttpErrorResponse) {

    }

    handle500Error(event: HttpResponse<any> | HttpErrorResponse) {

    }

}
