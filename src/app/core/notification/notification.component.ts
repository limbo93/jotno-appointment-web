import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationService, NotificationEvent, NotificationType } from './notification.service';
import { MessageService } from 'primeng/api';


@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html'
})
export class AppNotificationComponent implements OnInit, OnDestroy {

    private notificationSubscription: Subscription;

    constructor(private notificationService: NotificationService, private messageService: MessageService) { }

    ngOnInit() {
        this.notificationSubscription = this.notificationService.notifications$.subscribe(
            (notification) => {
                this.showNotification(notification);
            }
        )
    }

    ngOnDestroy() {
        this.notificationSubscription.unsubscribe();
    }

    showNotification(notification: NotificationEvent) {

        switch (notification.type) {
            case NotificationType.ALERT:
                this.messageService.add({ severity: 'error', summary: notification.title, detail: notification.message });
                break;

            case NotificationType.INFO:
                this.messageService.add({ severity: 'info', summary: notification.title, detail: notification.message });
                break;

            case NotificationType.SUCCESS:
                this.messageService.add({ severity: 'success', summary: notification.title, detail: notification.message });
                break;

            default:
                break;
        }
    }


}
