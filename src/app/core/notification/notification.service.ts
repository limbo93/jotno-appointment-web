import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

export enum NotificationType {
    SUCCESS, INFO, ALERT
}

export interface NotificationEvent {
    type: NotificationType,
    title?: string,
    message: string
}

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor() { }

    private notificationSource = new Subject<NotificationEvent>();

    notifications$ = this.notificationSource.asObservable();

    send(notification: NotificationEvent) {
        this.notificationSource.next(notification);
    }

    sendSuccessMsg(msg: string, params?: any) {
        this.send({
            type: NotificationType.SUCCESS,
            title: "Success!",
            message: msg
        });
    }

    sendErrorMsg(msg: string, params?: any) {
        this.send({
            type: NotificationType.ALERT,
            title: "Error!",
            message: msg
        });
    }

    sendInfoMsg(msg: string, params?: any[]) {
        this.send({
            type: NotificationType.INFO,
            title: "Info!",
            message: msg
        });
    }

    showDebugMessage(msg: string) {
        this.send({
            type: NotificationType.INFO,
            title: "Debug",
            message: msg
        });
    }

    handleError(errorResponse: HttpErrorResponse) {
        switch (errorResponse.status) {
            case 400:
                if (errorResponse.error.errors.fieldErrors) {
                    let msg = '';
                    Object.keys(errorResponse.error.errors.fieldErrors).forEach(key => {
                        Object.keys(errorResponse.error.errors.fieldErrors[key]).forEach(code => {
                           msg += errorResponse.error.errors.fieldErrors[key][code] + '\n';
                        });
                    });
                    this.sendErrorMsg(msg);
                }
                break;

        }
    }

}