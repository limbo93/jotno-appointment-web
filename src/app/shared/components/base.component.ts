import { DomSanitizer } from '@angular/platform-browser';
import { OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscriber } from 'rxjs';
import * as moment from 'moment';


export class BaseComponent implements OnDestroy {

  subscribers: any = {};

  busyState = false;
  stateName: string = null;

  isEmptyOrNullString(value: string) { return value == null || value.trim() === '' };

  // Unsubscribe from all subscribers on component destroy
  ngOnDestroy() {
    for (let subscriberKey in this.subscribers) {
      let subscriber = this.subscribers[subscriberKey];
      if (subscriber instanceof Subscriber) {
        subscriber.unsubscribe();
      }
    }
  }

  // recursively mark form group controls as touched
  protected markFormGroupAsTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsDirty();
      if (control.controls) {
        control.controls.forEach(ctrl => this.markFormGroupAsTouched(ctrl));
      }
    });
  }

  // recursively mark form group controls as untouched
  protected markFormGroupAsUnTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsPristine();
      if (control.controls) {
        control.controls.forEach(ctrl => this.markFormGroupAsUnTouched(ctrl));
      }
    });
  }


  protected enableBusyState(stateName?: string) {
    this.busyState = true;
    if (stateName) { this.stateName = stateName; }
  }

  protected disableBusyState() {
    this.busyState = false;
    this.stateName = null;
  }

  indexTrackerFn(item, index) {
    return index;
  }

}
